# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = 'e43d2322f38e19bda9a1d7d150e480669e6d430f77053a17ea31fa6390888d443295144bb5658fa8cda9bddbbf354550fc227c1e0b61a45ee05e30bcf584ce6c'
